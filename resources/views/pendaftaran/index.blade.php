@extends('template')

@section('main')
    <div class="row mb-4">
        <div class="col-12 text-center">
            <img style="width:250px;" src="{{ asset('custom/images/siaplight.png') }}" />
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card rounded-custom-1">
                <div class="card-body">
                    <div class="text-center mb-3">
                        <h2 class="mb-sm-0">Pendaftaran NPWPD Online</h2>
                    </div>
                    <div class="col-lg-8 mt-4">
                        <a class="btn btn-sm btn-danger" href="https://docs.google.com/document/d/1NUchayQiBJcsnLEqOOGkqr7eh4TdjQ8L/edit?usp=sharing&ouid=114948808253213586091&rtpof=true&sd=true">Panduan WP Khusus OPD dan Sekolah</a>
                         <a class="btn btn-sm btn-warning" href="https://docs.google.com/document/d/12RRWM_BfIIE8X_0R5HRcbi8KtvuqujRa/edit?usp=sharing&ouid=114948808253213586091&rtpof=true&sd=true">Panduan WP Umum</a>
                    </div>

                    <div>
                        <form id="form-register" method="POST" action="/pendaftaran/storependaftarannpwpd" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="row">
                                <div class="col-lg-8 mt-3">
                                    <label class="form-label">Nama Induk Usaha (Wajib Pajak)</label>
                                    <input class="form-control" type="text" name="nama_wp" id="nama_wp" placeholder="Nama Induk Usaha">
                                </div>
                                <div class="col-lg-4 mt-3">
                                    <label class="form-label">Badan Usaha</label>
                                    <select class="form-control" name="badan_usaha" id="badan_usaha">
                                        <option value="">Pilih</option>
                                        <option value="1">Badan Usaha</option>
                                        <option value="2">Pribadi</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-8 mt-3">
                                    <label class="form-label">Alamat</label>
                                    <textarea class="form-control" rows="3" id="alamat_wp" name="alamat_wp" placeholder="Alamat"></textarea>
                                </div>
                                <div class="col-md-2 mt-3">
                                    <label class="form-label">RT</label>
                                    <input class="form-control" type="text" maxlength="3" id="rt_wp" name="rt_wp" placeholder="RT">
                                </div>
                                <div class="col-md-2 mt-3">
                                    <label class="form-label">RW</label>
                                    <input class="form-control" type="text" maxlength="3" id="rw_wp" name="rw_wp" placeholder="RW">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mt-3">
                                    <label class="form-label">Provinsi</label><br />
                                    <select class='form-control select2' id='kode_provinsi_wp' name='kode_provinsi_wp'>
                                        @foreach ($provinsi as $val) {
                                            <option value="{{ $val->kode_provinsi }}" data-id="{{ $val->kode_provinsi }}" {{ $val->kode_provinsi == '33' ? 'selected="selected"' : '' }}>{{ $val->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mt-3">
                                    <label class="form-label">Kabupaten/Kota</label>
                                    <select class='form-control select2' id='kode_kota_wp' name='kode_kota_wp'>
                                    </select>
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label">Kecamatan</label>
                                    <select class='form-control select2' id='kode_kecamatan_wp' name='kode_kecamatan_wp'>
                                        <option value="">Pilih Kecamatan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mt-3">
                                    <label class="form-label">Desa/Kelurahan</label>
                                    <select class='form-control select2' id='kode_kelurahan_wp' name='kode_kelurahan_wp'>
                                        <option value="">Pilih Desa/Kelurahan</option>
                                    </select>
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label">Kode Pos</label>
                                    <input class="form-control" type="text" id="kode_pos_wp" name="kode_pos_wp" placeholder="Kode Pos">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mt-3">
                                    <label class="form-label">No Telp./HP WP</label>
                                    <input class="form-control" type="text" id="notel_wp" name="notel_wp" placeholder="No Telp/HP WP">
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label">E-mail WP</label>
                                    <input class="form-control" type="text" id="email_wp" placeholder="Email" name="email_wp">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mt-3">
                                    <label class="form-label">NIK</label>
                                    <input class="form-control" type="text" id="nik_pemilik" placeholder="NIK" name="nik_pemilik">
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label">Nama Pemilik</label>
                                    <input class="form-control" type="text" id="nama_pemilik" placeholder="Nama Pemilik" name="nama_pemilik">
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-6 mt-3">
                                    <label class="form-label">Alamat Pemilik</label>
                                    <textarea class="form-control" rows="3" id="alamat_pemilik" name="alamat_pemilik" placeholder="Alamat Pemilik"></textarea>
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label">No Telp/HP Pemilik</label>
                                    <input class="form-control" type="text" id="notel_pemilik" name="notel_pemilik" placeholder="No Telp/HP Pemilik">
                                </div>
                            </div>
                            <br /><br />
                            <button type='submit' class='btn btn-primary btn-lg waves-effect waves-light col-md-12' id='simpanlaporan'>SIMPAN DATA</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIcfDt4yUMeAiGKDTNyIufrBMuif-efms&libraries=places&callback=initMap" async defer></script>
    <script src="{{ asset('custom/js/register.js') }}"></script>
@endsection
