<!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <title>SIAP PAKDE - KABUPATEN TEMANGGUNG</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="SIMPATDA - KABUPATEN TEMANGGUNG" name="description" />
    <meta content="Themesbrand" name="author" />
    {{-- CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/logotmg.png') }}">

    <link href="{{ asset('assets/libs/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Bootstrap Css -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="{{ asset('assets/css/app.min.css') }}" id="app-style" rel="stylesheet" type="text/css" />

    <!-- Sweet Alert-->
    <link href="{{ asset('assets/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Dropzone -->
    <link href="{{ asset('assets/libs/dropzone/min/dropzone.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- DataTables -->
    <link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/libs/@chenfengyuan/datepicker/datepicker.min.css') }}">
    <link href="{{ asset('assets/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Table Responsive -->
    <link href="{{ asset('assets/libs/admin-resources/rwd-table/rwd-table.min.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('custom/css/app.css') }}" rel="stylesheet" type="text/css" />
</head>

<body data-sidebar="dark">
    {{-- <div id="preloader">
        <div id="status">
            <div class="spinner-chase">
                <div class="chase-dot"></div>
                <div class="chase-dot"></div>
                <div class="chase-dot"></div>
                <div class="chase-dot"></div>
                <div class="chase-dot"></div>
                <div class="chase-dot"></div>
            </div>
        </div>
    </div> --}}

    <div id="layout-wrapper">
        <div class="page-content pt-5">
            <div class="container">
                @include('sweetalert::alert')

                @yield('main')
            </div>
        </div>
    </div>

    <!-- JAVASCRIPT -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="{{ asset('assets/libs/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/libs/metismenu/metisMenu.min.js') }}"></script>
    <script src="{{ asset('assets/libs/simplebar/simplebar.min.js') }}"></script>
    <script src="{{ asset('assets/libs/node-waves/waves.min.js') }}"></script>
    <script src="{{ asset('assets/libs/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>
    <script src="{{ asset('assets/libs/@chenfengyuan/datepicker/datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>

    <!-- form advanced init -->
    <script src="{{ asset('assets/js/pages/form-advanced.init.js') }}"></script>

    <!-- Sweet Alerts js -->
    <script src="{{ asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>

    <!-- Sweet alert init js-->
    <script src="{{ asset('assets/js/pages/sweet-alerts.init.js') }}"></script>

    {{-- Jquery Validation --}}
    <script src="{{ asset('custom/plugin/jquery-validation@1.19.3/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('custom/plugin/jquery-validation@1.19.3/additional-methods.min.js') }}"></script>

    @yield('script')

    <script type="text/javascript">
        $(document).ready(function() {
            $('#kode_provinsi_wp').change(function() {
                var token = $("input[name='_token']").val();
                var kode_prov = $(this).val();

                $.ajax({
                    type: 'POST',
                    url: '/pendaftaran/getDataKota',
                    data: {
                        _token: token,
                        kode_prov: kode_prov
                    },
                    success: function(data) {
                        $('#kode_kota_wp').html(data);
                    }
                });
            });

            $('#kode_provinsi_objek').change(function() {
                var token = $("input[name='_token']").val();
                var kode_prov = $(this).val();

                $.ajax({
                    type: 'POST',
                    url: '/pendaftaran/getDataKota',
                    data: {
                        _token: token,
                        kode_prov: kode_prov
                    },
                    success: function(data) {
                        $('#kode_kota_objek').html(data);
                    }
                });
            });

            $('#kode_kota_wp').change(function() {
                var token = $("input[name='_token']").val();
                var kode_kota = $(this).val();
                var kode_prov = $('#kode_kota_wp option:selected').data('prov');

                $.ajax({
                    type: 'POST',
                    url: '/pendaftaran/getDataKecamatan',
                    data: {
                        _token: token,
                        kode_kota: kode_kota,
                        kode_prov: kode_prov
                    },
                    success: function(data) {
                        $('#kode_kecamatan_wp').html(data);
                    }
                });
            });

            $('#kode_kota_objek').change(function() {
                var token = $("input[name='_token']").val();
                var kode_kota = $(this).val();
                var kode_prov = $('#kode_kota_objek option:selected').data('prov');

                $.ajax({
                    type: 'POST',
                    url: '/pendaftaran/getDataKecamatan',
                    data: {
                        _token: token,
                        kode_kota: kode_kota,
                        kode_prov: kode_prov
                    },
                    success: function(data) {
                        $('#kode_kecamatan_objek').html(data);
                    }
                });
            });

            $('#kode_kecamatan_wp').change(function() {
                var token = $("input[name='_token']").val();
                var kode_kec = $(this).val();
                var kode_prov = $('#kode_kecamatan_wp option:selected').data('prov');
                var kode_kota = $('#kode_kecamatan_wp option:selected').data('kota');

                $.ajax({
                    type: 'POST',
                    url: '/pendaftaran/getDataKelurahan',
                    data: {
                        _token: token,
                        kode_kec: kode_kec,
                        kode_kota: kode_kota,
                        kode_prov: kode_prov
                    },
                    success: function(data) {
                        $('#kode_kelurahan_wp').html(data);
                    }
                });
            });

            $('#kode_kecamatan_objek').change(function() {
                var token = $("input[name='_token']").val();
                var kode_kec = $(this).val();
                var kode_prov = $('#kode_kecamatan_objek option:selected').data('prov');
                var kode_kota = $('#kode_kecamatan_objek option:selected').data('kota');

                $.ajax({
                    type: 'POST',
                    url: '/pendaftaran/getDataKelurahan',
                    data: {
                        _token: token,
                        kode_kec: kode_kec,
                        kode_kota: kode_kota,
                        kode_prov: kode_prov
                    },
                    success: function(data) {
                        $('#kode_kelurahan_objek').html(data);
                        $('#kode_kelurahan_objek').html(data);
                    }
                });
            });

            $('#kode_kelurahan_wp').change(function() {
                var token = $("input[name='_token']").val();
                var kode_kel = $(this).val();
                var kode_prov = $('#kode_provinsi_wp option:selected').val();
                var kode_kota = $('#kode_kota_wp option:selected').val();
                var kode_kec = $('#kode_kecamatan_wp option:selected').val();

                $.ajax({
                    type: 'POST',
                    url: '/pendaftaran/getDataKodePos',
                    data: {
                        _token: token,
                        kode_kel: kode_kel,
                        kode_kec: kode_kec,
                        kode_kota: kode_kota,
                        kode_prov: kode_prov
                    },
                    success: function(data) {
                        $('#kode_pos_wp').val(data.kode_pos);
                    }
                });
            });

            $('#kode_kelurahan_objek').change(function() {
                var token = $("input[name='_token']").val();
                var kode_kel = $(this).val();
                var kode_prov = $('#kode_provinsi_objek option:selected').val();
                var kode_kota = $('#kode_kota_objek option:selected').val();
                var kode_kec = $('#kode_kecamatan_objek option:selected').val();

                $.ajax({
                    type: 'POST',
                    url: '/pendaftaran/getDataKodePos',
                    data: {
                        _token: token,
                        kode_kel: kode_kel,
                        kode_kec: kode_kec,
                        kode_kota: kode_kota,
                        kode_prov: kode_prov
                    },
                    success: function(data) {
                        $('#kode_pos_objek').val(data.kode_pos);
                    }
                });
            });
        });

        function numbersonly(myfield, e, dec) {
            var key;
            var keychar;

            if (window.event)
                key = window.event.keyCode;
            else if (e)
                key = e.which;
            else
                return true;
            keychar = String.fromCharCode(key);
            // control keys
            if ((key == null) || (key == 0) || (key == 8) ||
                (key == 9) || (key == 13) || (key == 27))
                return true;

            // numbers
            else if ((("0123456789").indexOf(keychar) > -1))
                return true;

            // decimal point jump
            else if (dec && (keychar == ".")) {
                myfield.form.elements[dec].focus();
                return false;
            } else
                return false;
        }
    </script>
</body>

</html>
