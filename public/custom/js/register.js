// setting
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function getNikCapil() {
    var token = $("input[name='_token']").val();
    var nik = $("#nik_pemilik").val();
    var badanUsaha = $("#badan_usaha").val();
    console.log(badanUsaha);

    if (badanUsaha == '1') {
        $.ajax({
            type: 'POST',
            url: '/pendaftaran/getnik',
            data: {
                _token: token,
                nik: nik
            },
            success: function (response) {
                if (response.success) {
                    $('#alamat_pemilik').val(response.data.alamat_pemilik);
                    $('#nama_pemilik').val(response.data.nama_pemilik);
                    $('#notel_pemilik').val(response.data.no_telepon);

                    $('#alamat_pemilik, #nama_pemilik, #notel_pemilik').attr('disabled', true);
                } else {
                    $('#alamat_pemilik').val('');
                    $('#nama_pemilik').val('');
                    $('#notel_pemilik').val('');

                    $('#alamat_pemilik, #nama_pemilik, #notel_pemilik').removeAttr('disabled');
                }
            }
        });
    }
}


function getdatakota(kode_prov) {
    var token = $("input[name='_token']").val();

    $.ajax({
        type: 'POST',
        url: '/pendaftaran/getDataKota',
        data: {
            _token: token,
            kode_prov: kode_prov
        },
        success: function (data) {
            $('#kode_kota_wp').html(data);
            $('#kode_kota_objek').html(data);
            var kode_kota = $("#kode_kota_wp option:selected").val();
            setTimeout(getdatakecamatan(kode_prov, kode_kota), 100);
        }
    });
}

function getdatakecamatan(kode_prov, kode_kota) {
    var token = $("input[name='_token']").val();

    $.ajax({
        type: 'POST',
        url: '/pendaftaran/getDataKecamatan',
        data: {
            _token: token,
            kode_prov: kode_prov,
            kode_kota: kode_kota
        },
        success: function (data) {
            $('#kode_kecamatan_wp').html(data);
            $('#kode_kecamatan_objek').html(data);
        }
    });
}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else {
        alert('Browser yang anda gunakan tidak support.');
    }
}

function showPosition(p) {
    var lat = parseFloat(p.coords.latitude.toFixed(7));
    var lng = parseFloat(p.coords.longitude.toFixed(7));

    $('#latitudeobjek').val(lat);
    $('#longitudeobjek').val(lng);

    document.getElementById("tempatmap").innerHTML = "<input type='text' name='searchMapInput' id='searchMapInput' class='mapControls form-control'>";
    initMap();
    dataAlamat(lat, lng);
}

function showError(e) {
    switch (e.code) {
        case e.PERMISSION_DENIED:
            alert('Ijinkan untuk akses Geolocation (GPS) untuk mendapatkan data lokasi anda.')
            break;
    }
}

function initMap() {
    var lat = parseFloat($('#latitudeobjek').val());
    var lng = parseFloat($('#longitudeobjek').val());

    if (isNaN(lat) || isNaN(lng)) {
        lat = parseFloat(-7.3337685);
        lng = parseFloat(110.1801539);
    }

    var myLatLng = {
        lat: lat,
        lng: lng
    };

    var map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: lat,
            lng: lng
        },
        zoom: 17
    });

    var input = document.getElementById('searchMapInput');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        map: map,
        position: myLatLng,
        draggable: true
    });

    autocomplete.addListener('place_changed', function () {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();

        /* If the place has a geometry, then present it on a map. */
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }

        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        /* Location details */
        $('#latitudeobjek').val(place.geometry.location.lat().toFixed(7));
        $('#longitudeobjek').val(place.geometry.location.lng().toFixed(7));
        dataAlamat(place.geometry.location.lat(), place.geometry.location.lng());
    });

    google.maps.event.addListener(marker, 'dragend', function (marker) {
        var latLng = marker.latLng;
        $('#latitudeobjek').val(latLng.lat().toFixed(7));
        $('#longitudeobjek').val(latLng.lng().toFixed(7));
        dataAlamat(latLng.lat(), latLng.lng());
    });
}

function dataAlamat(lat, lng) {
    var latlng = {
        lat: lat,
        lng: lng
    };
    var geocoder = new google.maps.Geocoder;
    geocoder.geocode({
        'location': latlng
    }, function (results, status) {
        if (status === 'OK') {
            if (results[0]) {
                rs = results[0].formatted_address;
                rs = rs.replace('Kec.', '');
                var arrval = rs.split(",");
                arrval[2] = arrval[2].replace('.', '');
                $('#searchMapInput').val(results[0].formatted_address);
            } else {
                rs = 'No results found';
                alert(rs);
            }
        } else {
            rs = 'Geocoder failed due to: ' + status;
            alert(rs);
        }
    });
}

$(function () {
    'use strict';

    var kode_prov = $("#kode_provinsi_wp option:selected").val();

    getLocation();
    getdatakota(kode_prov);

    $('#jenis_pajak_id').change(function () {
        var token = $("input[name='_token']").val();
        var jenispajakid = $(this).val();

        $.ajax({
            type: 'POST',
            url: '/pendaftaran/getcomborekening',
            data: {
                _token: token,
                jenis_pajak_id: jenispajakid
            },
            success: function (data) {
                $('#rekening_id').html(data);
            }
        });
    });

    // $('#nik_pemilik').keyup(function () {
    //     $('#messagenik').text("");
    // });
    $('#nik_pemilik').blur(function () {
        getNikCapil();
    });

    $('#form-register').validate({
        rules: {
            nama_wp: 'required',
            badan_usaha: 'required',
            alamat_wp: 'required',
            rt_wp: 'required',
            rw_wp: 'required',
            kode_provinsi_wp: 'required',
            kode_kota_wp: 'required',
            kode_kecamatan_wp: 'required',
            kode_kelurahan_wp: 'required',
            kode_pos_wp: 'required',
            notel_wp: 'required',
            email_wp: 'required',
            nama_pemilik: 'required',
            nik_pemilik: {
                required: true,
                remote: {
                    url: '/pendaftaran/ceknik',
                    type: 'post',
                    data: {
                        badan_usaha: function () {
                            return $('#badan_usaha').val();
                        }
                    },
                }
            },
            alamat_pemilik: 'required',
            notel_pemilik: 'required',
        },
        messages: {
            nama_wp: 'Nama induk usaha harus diisi',
            badan_usaha: 'Badan usaha harus diisi',
            alamat_wp: 'Alamat harus diisi',
            rt_wp: 'RT harus diisi',
            rw_wp: 'RW harus diisi',
            kode_provinsi_wp: 'Provinsi harus diisi',
            kode_kota_wp: 'Kota/kabupaten harus diisi',
            kode_kecamatan_wp: 'Kecamatan harus diisi',
            kode_kelurahan_wp: 'Kelurahan harus diisi',
            kode_pos_wp: 'Kode pos harus diisi',
            notel_wp: 'Nomor telepon harus diisi',
            email_wp: 'Email harus diisi',
            nama_pemilik: 'Nama pemilik harus diisi',
            nik_pemilik: {
                required: 'NIK pemilik harus diisi',
                remote: 'NIK pemilik sudah ada / Pastikan 1 NIK untuk 1 Pribadi dan 1 Badan Usaha'
            },
            alamat_pemilik: 'Alamat pemilik harus diisi',
            notel_pemilik: 'Nomor telepon pemilik harus diisi',
        },
        errorPlacement: function (label, element) {
            if (element.hasClass('select2') || element.hasClass('select2-show-search')) {
                $(element).siblings('span:first()').append(label);
            } else {
                $(label).insertAfter(element);
            }
        },
        onkeyup: function () {
            $('#simpanlaporan').removeClass('disabled');
        }
    });

    $('#badan_usaha').change(function (e) {
        var nik = $('#nik_pemilik').val();
        var badanUsaha = $('#badan_usaha').val();
        if (nik) {
            if ($('#nik_pemilik').valid()) {
                if (badanUsaha) {
                    getNikCapil();
                }
            }
        }
    });

    $('#kode_provinsi_wp, #kode_kota_wp, #kode_kecamatan_wp, #kode_kelurahan_wp').change(function (e) {
        e.preventDefault();
        var id = $(this).attr('id');
        $('#' + id).removeClass('error');
        $('#' + id + '-error').remove();
    });

    $('#form-register').submit(function (e) {
        if ($('#form-register').valid()) {
            $('#simpanlaporan').addClass('disabled');
        }
    });
});
