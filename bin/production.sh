sudo composer install --optimize-autoloader --no-dev
sudo php artisan optimize:clear
sudo composer dump-autoload -o
sudo php artisan config:cache
sudo php artisan route:cache
sudo php artisan view:cache
sudo chmod -R 777 storage/
