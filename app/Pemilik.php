<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pemilik extends Model
{
    protected $table = 'pemilik';
    protected $primaryKey = 'id';

    protected $fillable = [
        'nama_pemilik',
        'alamat_pemilik',
        'nik_pemilik',
        'no_telepon',
        'status_online',
        'created_at',
        'updated_at'
    ];
}
