<?php

namespace App\Http\Controllers;

use App\Helper;
use App\ObjekPajakBaru;
use App\Pemilik;
use App\PemilikBaru;
use App\PengelolaBaru;
use App\Rekening;
use App\WajibPajak;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use RealRashid\SweetAlert\Facades\Alert;

class Pendaftaran extends Controller
{
    public function index()
    {
        $datakec = new Helper();
        $provinsi = $datakec->getDataProvinsi();

        return view('pendaftaran.index', compact('provinsi'));
    }

    public function getComboRekening(Request $request)
    {
        $datarek = new Rekening();
        $jenis_pajak_id = $request->input('jenis_pajak_id');
        $datas = $datarek->getComboRekening($jenis_pajak_id);
        $html = "<option value=''>Pilih</option>";

        foreach ($datas as $val) {
            $html .= "<option value='$val->id'>$val->nama_rek</option>";
        }

        return response()->json($html);
    }

    public function storependaftarannpwpd(Request $request)
    {
        $objekpajak = new ObjekPajakBaru();

        DB::beginTransaction();

        $tgldaftar = date('Y-m-d');
        $nama_wp = $request->nama_wp;
        $alamat_wp = $request->alamat_wp;
        $notel_wp = $request->notel_wp;
        $email_wp = $request->email_wp;
        $rt_wp = $request->rt_wp;
        $rw_wp = $request->rw_wp;
        $jenis_pajak_id = $request->input('jenis_pajak_id');
        $rekening_id = $request->input('rekening_id');
        $kode_kecamatan_wp = $request->input('kode_kecamatan_wp');
        $kode_kelurahan_wp = $request->input('kode_kelurahan_wp');
        $kode_kota_wp = $request->input('kode_kota_wp');
        $kode_provinsi_wp = $request->input('kode_provinsi_wp');
        $kode_pos_wp = $request->kode_pos_wp;
        $nama_pemilik = $request->nama_pemilik;
        $alamat_pemilik = $request->alamat_pemilik;
        $nik_pemilik = $request->nik_pemilik;
        $notel_pemilik = $request->notel_pemilik;
        $badan_usaha = $request->input('badan_usaha');

        try {
            $queryPemilik = Pemilik::where('nik_pemilik', $nik_pemilik);
            if ($queryPemilik->exists()) {
                $queryPemilik->update([
                    "nama_pemilik" => $nama_pemilik,
                    "alamat_pemilik" => $alamat_pemilik,
                    "nik_pemilik" => $nik_pemilik,
                    "no_telepon" => $notel_pemilik,
                ]);

                $pemilik = Pemilik::where('nik_pemilik', $nik_pemilik)->first();
            } else {
                $pemilik = Pemilik::create([
                    "nama_pemilik" => $nama_pemilik,
                    "alamat_pemilik" => $alamat_pemilik,
                    "nik_pemilik" => $nik_pemilik,
                    "no_telepon" => $notel_pemilik,
                    "status_online" => 1
                ]);
            }

            $queryWajibPajak = WajibPajak::where('badan_usaha', $badan_usaha)
                ->where('pemilik_id', $pemilik->id);
            if ($queryWajibPajak->exists()) throw new Exception('Gagal');

            $wajibpajak = WajibPajak::create([
                "tgl_daftar" => $tgldaftar,
                "nama_wp" => $nama_wp,
                "badan_usaha" => $badan_usaha,
                "alamat_wp" => $alamat_wp,
                "no_telepon" => $notel_wp,
                "email_wp" => $email_wp,
                "rt_wp" => $rt_wp,
                "rw_wp" => $rw_wp,
                "kode_kecamatan" => $kode_kecamatan_wp,
                "kode_kelurahan" => $kode_kelurahan_wp,
                "kode_kota" => $kode_kota_wp,
                "kode_provinsi" => $kode_provinsi_wp,
                "kode_pos" => $kode_pos_wp,
                "pemilik_id" => $pemilik->id,
                "operator_daftar" => 999,
                "status" => 1,
                "status_online" => 1
            ]);

            DB::commit();

            Alert::success('Berhasil', 'Data Berhasil Disimpan');

            return redirect::to('/');
        } catch (\Throwable $t) {
            DB::rollback();
            Alert::error('Gagal', 'Data Gagal Disimpan');
            return redirect::to('/');
        }
    }

    public function ceknik(Request $request)
    {
        $nik = $request->input('nik_pemilik');
        $badanUsaha = $request->input('badan_usaha');

        $queryPemilik = Pemilik::where('nik_pemilik', $nik);
        if ($queryPemilik->exists()) {
            $pemilik = $queryPemilik->first();

            $queryWajibPajak = WajibPajak::where('badan_usaha', $badanUsaha)
                ->where('pemilik_id', $pemilik->id);
            if ($queryWajibPajak->exists()) {
                return response()->json(false);
            }
        }

        return response()->json(true);
    }

    public function getNik(Request $request)
    {
        try {
            $nik = $request->input('nik');

            $queryPemilik = Pemilik::where('nik_pemilik', $nik);
            if ($queryPemilik->exists()) {
                $pemilik = $queryPemilik->first();

                $data = [
                    'success' => true,
                    'data' => $pemilik
                ];
            } else {
                throw new Exception('Data tidak ditemukan');
            }
        } catch (Exception $e) {
            $data = [
                'success' => false,
                'data' => null
            ];
        }

        return response()->json($data);
    }
}
